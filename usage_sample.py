import MyOwnHarvester

def main():
    myharv = MyOwnHarvester.HTMLHarvester()
    myharv.doHarvesting("http://kalorycznosc.eu.interiowo.pl/Drob.html")
    myharv.convertAllItemsFromISO2UTF()

    myharv.printLastHarvested()
    myitems = myharv.getLastHarvested()

if __name__ == "__main__" :
    main()

