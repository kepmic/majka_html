import requests

class Item:
    def __init__(self, input_list):
        self.nazwa = input_list[0]
        self.kcal = int(float(input_list[1].replace(",",".")))
        self.bialka = float(input_list[2].replace(",","."))
        self.tluszcze = float(input_list[3].replace(",","."))
        self.wegle = float(input_list[4].replace(",","."))

    def __str__(self):
        return "%s | %d | %.1f | %.1f | %.1f " % (self.nazwa, self.kcal, self.bialka, self.tluszcze, self.wegle)

class HTMLHarvester:
    def __init__(self, html_table_to_harvest = 3, remove_first_row = True):
        '''
        html_table_to_harvest   - number of table from html site to work on ("1" indexed)
        remove_first_row        - removing first row (as it may contains only descriptions)
        '''
        self.html_table_to_harvest = html_table_to_harvest
        self.remove_first_row = remove_first_row
        self.items = []
        """:type : list[Item]"""

    @staticmethod
    def _getSection(text_data, begin_marker, end_marker):
        result = []
        index_end = 0
        while True:
            index_begin = text_data.find(begin_marker, index_end)
            if index_begin != -1:
                index_end = text_data.find(end_marker, index_begin)
                result.append(text_data[index_begin:index_end])

            else:
                return result

    @staticmethod
    def _getRidOfScrapsFromStringList(string_list, scrap_list):
        '''
        input_list  - list of strings
        scrap_list  - list of strings which would be removed from every string in input_list'''
        for i in xrange(len(string_list)):
            for scrap in scrap_list:
                string_list[i] = string_list[i].replace(scrap, "")

    def doHarvesting(self, www_address):
        '''Main method to harvest data.'''
        data = requests.get(www_address).content
        self.items = []

        tables = self._getSection(data, "<table", "</table")
        rows = self._getSection(tables[self.html_table_to_harvest - 1], "<tr", "</tr")
        if self.remove_first_row:
            rows.pop(0)
        for row in rows:
            cells = self._getSection(row, "<td", "</td")
            self._getRidOfScrapsFromStringList(cells,["<td>", "\n", "\r"])
            self.items.append(Item(cells))

    def printLastHarvested(self):
        for item in self.items:
            print item

    def getLastHarvested(self):
        return self.items

    def convertAllItemsFromISO2UTF(self):
        for item in self.items:
            item.nazwa = item.nazwa.decode("iso-8859-2").encode("utf8")


